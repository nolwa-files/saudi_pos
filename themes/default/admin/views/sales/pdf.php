<?php defined('BASEPATH') or exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $this->lang->line('sale') . ' ' . $inv->reference_no; ?></title>
    <link href="<?= $assets ?>styles/pdf/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $assets ?>styles/pdf/pdf.css" rel="stylesheet">
    <style>
    .qrimg{
        height : 140px;
    }
    .bg-dark{
        background-color: #ddd;
    }
    .text-left{
        text-align: left !important;
    }
    .text-right{
        text-align: right !important;
    }
</style>
</head>

<body>
<div id="wrap">
    <div class="row">
        <div class="col-lg-12">
            <?php if ($logo) {
    $path   = base_url() . 'assets/uploads/logos/' . $biller->logo;
    $type   = pathinfo($path, PATHINFO_EXTENSION);
    $data   = file_get_contents($path);
    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data); ?>
                <div class="text-center" style="margin-bottom:20px;">
                    <img src="<?= $base64; ?>" alt="<?= $biller->company && $biller->company != '-' ? $biller->company : $biller->name; ?>">
                </div>
            <?php
}
            ?>
            <div class="clearfix"></div>
            <div class="padding10">
                <?php if ($Settings->invoice_view == 1) {
                ?>
                    <div class="col-xs-12 text-center">
                        <h1><?= lang('tax_invoice'); ?></h1>
                    </div>
                <?php
            } ?>

                <div class="col-xs-8">
                <div class="table-responsive">
                                <table class="table table-bordered print-table order-table">
                                 <tr>
                                     <td>Invoice Number:</td>
                                     <td> <?= $inv->id; ?> </td>
                                     <td class="text-right"> <?= $inv->id; ?> </td>
                                     <td class="text-right" style="direction: rtl;">رقم الفاتورة:</td>
                                 </tr>
                             </table>
                         </div>
                         <div class="table-responsive">
                                <table class="table table-bordered print-table order-table">
                                 <tr>
                                     <td>Invoice Issue Date:</td>
                                     <td><?=  date("d/m/Y", strtotime($inv->date)); ?></td>
                                     <td class="text-right"><?=  date("d/m/Y", strtotime($inv->date)); ?></td>
                                     <td class="text-right" style="direction: rtl;">تاريخ إصدار الفاتورة:</td>
                                 </tr>
                                 <tr>
                                     <td>Date of Supply:</td>
                                     <td><?= $delivery->date ?  date("d/m/Y", strtotime($delivery->date)) : date("d/m/Y", strtotime($inv->date)); ?></td>
                                     <td>
                                     <?= $delivery->date ?  date("d/m/Y", strtotime($delivery->date)) : date("d/m/Y", strtotime($inv->date)); ?></td>
                                     <td style="direction: rtl;">تاريخ التوريد:</td>
                                 </tr>
                             </table>
                         </div>
                </div>

                <div class="col-xs-4">
                <div class="order_barcodes text-center">
                <?php 
                            $inv_date = date("m/d/Y h:i:s A", strtotime($inv->date));
                            $vat_total = $this->sma->formatMoney($return_sale ? (($inv->grand_total + $return_sale->grand_total) - ($inv->order_discount + $return_sale->order_discount)) * 15 / 100 : ($inv->total - $inv->order_discount)* 15 / 100);
                            $grand_total = $this->sma->formatMoney($return_sale ? ((($inv->grand_total + $return_sale->grand_total) - ($inv->order_discount + $return_sale->order_discount)) * 15 / 100) + (($inv->grand_total + $return_sale->grand_total) - ($inv->order_discount + $return_sale->order_discount)) : (($inv->total - $inv->order_discount)* 15 / 100) + ($inv->total - $inv->order_discount));
                            $seller = $biller->company;
                            $result = chr(1) . chr( strlen($seller) ) . $seller;
                            $result.= chr(2) . chr( strlen($biller->vat_no) ) . $biller->vat_no;
                            $result.= chr(3) . chr( strlen($inv_date) ) . $inv_date;
                            $result.= chr(4) . chr( strlen($grand_total) ) . $grand_total;
                            $result.= chr(5) . chr( strlen($vat_total) ) . $vat_total;
                            $newQr = base64_encode($result); ?>
                            <?= $this->sma->qrcode('url', $newQr, 0); ?>
                    </div>
                </div>

            </div>

            <div class="clearfix"></div>
            <div class="col-xs-12" style="margin-top: 15px;">
            <div class="table-responsive">
                                <table class="table table-bordered print-table order-table">
                                    <thead>
                                        <tr class="bg-dark">
                                            <th class="text-left" colspan="2">Seller:</th>
                                            <th class="text-right" colspan="2" style="direction: rtl;">تاجر:</th>
                                            <th class="text-left" colspan="2">Buyer:</th>
                                            <th class="text-right" colspan="2" style="direction: rtl;">مشتر:</th>
                                        </tr>
                                </thead>
                                <tbody>
                                 <tr >
                                     <td>Name:</td>
                                     <td colspan="2"><?= $biller->company && $biller->company != '-' ? $biller->company : $biller->name; ?></td>
                                     <td class="text-right" style="direction: rtl;">اسم:</td>
                                     <td>Name:</td>
                                     <td  colspan="2"><?= $customer->company && $customer->company != '-' ? $customer->company : $customer->name; ?></td>
                                     <td class="text-right" style="direction: rtl;">اسم:</td>
                                 </tr>
                                 <tr >
                                     <td>Building No:</td>
                                     <td colspan="2"><?= $biller->cf1?></td>
                                     <td class="text-right" style="direction: rtl;">عنوان:</td>
                                     <td>Building No:</td>
                                     <td  colspan="2"><?=$customer->cf1;?></td>
                                     <td class="text-right" style="direction: rtl;">عنوان:</td>
                                 </tr>
                                 <tr >
                                     <td>Street Name :</td>
                                     <td colspan="2"><?= $biller->cf2?></td>
                                     <td class="text-right" style="direction: rtl;">اسم الشارع:</td>
                                     <td>Street Name :</td>
                                     <td  colspan="2"><?=$customer->cf2;?></td>
                                     <td class="text-right" style="direction: rtl;">اسم الشارع:</td>
                                 </tr>
                                 <tr >
                                     <td>District :</td>
                                     <td colspan="2"><?= $biller->state; ?></td>
                                     <td class="text-right" style="direction: rtl;">يصرف:</td>
                                     <td>District :</td>
                                     <td  colspan="2"><?= $customer->state; ?></td>
                                     <td class="text-right" style="direction: rtl;">يصرف:</td>
                                 </tr>
                                 <tr >
                                     <td>City :</td>
                                     <td colspan="2"><?=$biller->city?></td>
                                     <td class="text-right" style="direction: rtl;">مدينة:</td>
                                     <td>City :</td>
                                     <td  colspan="2"><?=$customer->city?></td>
                                     <td class="text-right" style="direction: rtl;">مدينة:</td>
                                 </tr>
                                 <tr >
                                     <td>Country :</td>
                                     <td colspan="2"><?=$biller->country?></td>
                                     <td class="text-right" style="direction: rtl;">دولة:</td>
                                     <td>Country :</td>
                                     <td  colspan="2"><?=$customer->country?></td>
                                     <td class="text-right" style="direction: rtl;">دولة:</td>
                                 </tr>
                                 <tr >
                                     <td>Postal Code :</td>
                                     <td colspan="2"><?= $biller->postal_code?></td>
                                     <td class="text-right" style="direction: rtl;">رمز بريدي:</td>
                                     <td>Postal Code :</td>
                                     <td  colspan="2"><?= $customer->postal_code?></td>
                                     <td class="text-right" style="direction: rtl;">رمز بريدي:</td>
                                 </tr>
                                 <tr >
                                     <td>Additional No :</td>
                                     <td colspan="2"><?=$biller->cf3;?></td>
                                     <td class="text-right" style="direction: rtl;">رقم إضافي:</td>
                                     <td>Additional No :</td>
                                     <td  colspan="2"><?=$customer->cf3;?></td>
                                     <td class="text-right" style="direction: rtl;">رقم إضافي:</td>
                                 </tr>
                                 <tr >
                                     <td>VAT Number :</td>
                                     <td colspan="2"><?= $biller->vat_no?></td>
                                     <td class="text-right" style="direction: rtl;">ظريبه الشراء:</td>
                                     <td>VAT Number :</td>
                                     <td  colspan="2"><?= $customer->vat_no?></td>
                                     <td class="text-right" style="direction: rtl;">ظريبه الشراء:</td>
                                 </tr>
                                 <tr >
                                     <td>Other Seller ID :</td>
                                     <td colspan="2"><?=$biller->cf4;?></td>
                                     <td class="text-right" style="direction: rtl;">معرف البائع الآخر:</td>
                                     <td>Other Seller ID :</td>
                                     <td  colspan="2"><?=$customer->cf4;?></td>
                                     <td class="text-right" style="direction: rtl;">معرف البائع الآخر:</td>
                                 </tr>
                                </tbody>
                             </table>
                         </div>  
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12" style="margin-top: 15px;">
            <div class="table-responsive ">
                    <table class="table table-bordered table-hover table-striped print-table order-table">
                        <thead>
                        <tr>
                            <th colspan="4" class="text-left"><?= lang('Line_Items'); ?></th> <br/>
                            <th colspan="4" class="text-right" style="direction: rtl;">قائمة العناصر</th>
                        </tr>
                        </thead>
                        <thead>

                        <tr>
                            <th class="text-center"><span>Nature of goods
or services</span> <br/><span style="direction: rtl;">طبيعة السلع
أو الخدمات</span></th>
                            <th class="text-center"><span>Unit price</span><br/> <span style="direction: rtl;">سعر الوحدة</span></th>
                            <th class="text-center"><span>Quantity</span> <br/> <span style="direction: rtl;">كمية</span></th>
                            <?php
                            ?>
                            <th class="text-center"><span>Taxable Amount</span> <br/> <span style="direction: rtl;">المبلغ الخاضع للضريبة</span></th>
                            <th style="padding-right:20px; text-align:center; vertical-align:middle;">Discount<br/> <span style="direction: rtl;">خصم</span> </th>
                            <th class="text-center"><span>Tax Rate</span> <br/> <span style="direction: rtl;">معدل الضريبة</span></th>
                            <th class="text-center"><span>Tax Amount</span> <br/> <span style="direction: rtl;">قيمة الضريبة</span></th>
                            <th class="text-center"><span>Item Subtotal
(Including VAT)</span> <br/> <span style="direction: rtl;">المجموع الفرعي للعنصر
(بما في ذلك ضريبة القيمة المضافة)</span></th>
                            
                        </tr>

                        </thead>

                        <tbody>

                        <?php 
                        foreach ($rows as $row):
                            ?>
                            <tr>
                                <td style="text-align:center; vertical-align:middle;"><?= $row->product_code . ' - ' . $row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                    <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                    <?= $row->details ? '<br>' . $row->details : ''; ?></td>
                                <td style="vertical-align:middle;">
                                   <?= $this->sma->formatQuantity($row->net_unit_price); ?>
                                </td>
                                <td style="text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->unit_quantity) . ' ' . ($inv->sale_status == 'returned' ? $row->base_unit_code : $row->product_unit_code); ?></td>
                                <td style="text-align:center; padding-right:10px;">
                                    <?php $taxableAmount = $row->net_unit_price * $row->unit_quantity;?>
                                    <?= $this->sma->formatMoney($taxableAmount); ?>
                                </td>
                                <td style="text-align:center; padding-right:10px;">
                                    <?php echo($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) ?>
                                </td>
                                <td style="text-align:center;  padding-right:10px;"><?= $row->tax; ?></td>
                                <td style="text-align:center;  padding-right:10px;"><?php $totalVat =($taxableAmount - $row->item_discount) * ($row->tax_rate) / 100; ?>
                                <?= $this->sma->formatMoney($totalVat); ?></td>
                                <td style="text-align:center;  padding-right:10px;">
                                <?= $this->sma->formatMoney($row->subtotal); ?>
                                <!-- <?= $this->sma->formatMoney($taxableAmount + $totalVat); ?> -->
                            </td>
                            </tr>
                            <?php
                            $r++;
                        endforeach;

                        if ($return_rows) {
                            echo '<tr class="warning"><td colspan="100%" class="no-border"><strong>' . lang('returned_items') . '</strong></td></tr>';
                            foreach ($return_rows as $row):
                                ?>
                                <tr class="warning">
                                    <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                    <td style="vertical-align:middle;">
                                        <?= $row->product_code . ' - ' . $row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                        <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                        <?= $row->details ? '<br>' . $row->details : ''; ?>
                                    </td>
                                    <?php if ($Settings->indian_gst) {
                                    ?>
                                    <td style="width: 80px; text-align:center; vertical-align:middle;"><<?= $row->hsn_code ?: ''; ?></td>
                                    <?php
                                } ?>
                                    <td style="width: 100px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity) . ' ' . $row->base_unit_code; ?></td>
                                    <?php
                                    if ($Settings->product_serial) {
                                        echo '<td>' . $row->serial_no . '</td>';
                                    } ?>
                                    <td style="text-align:right; width:120px; padding-right:10px;"><?= $this->sma->formatMoney($row->unit_price); ?></td>
                                    <?php
                                    if ($Settings->tax1 && $inv->product_tax > 0) {
                                        echo '<td style="width: 120px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>(' . ($Settings->indian_gst ? $row->tax : $row->tax_code) . ')</small>' : '') . ' ' . $this->sma->formatMoney($row->item_tax) . '</td>';
                                    }
                            if ($Settings->product_discount && $inv->product_discount != 0) {
                                echo '<td style="width: 120px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) . '</td>';
                            } ?>
                                    <td style="text-align:right; width:120px; padding-right:10px;"><?= $this->sma->formatMoney($row->subtotal); ?></td>
                                </tr>
                                <?php
                                $r++;
                            endforeach;
                        }

                        ?>
                        </tbody>
                        
                        <tfoot>
                        <!-- <?php if ($inv->grand_total != $inv->total) {
                            ?>
                            <tr>
                                <td colspan="<?= $tcol; ?>"
                                    style="text-align:right; padding-right:10px;"><?= lang('total'); ?>
                                    (<?= $default_currency->code; ?>)
                                </td>
                                <?php
                                if ($Settings->tax1 && $inv->product_tax > 0) {
                                    echo '<td style="text-align:right;">' . $this->sma->formatMoney($return_sale ? ($inv->product_tax + $return_sale->product_tax) : $inv->product_tax) . '</td>';
                                }
                            if ($Settings->product_discount && $inv->product_discount != 0) {
                                echo '<td style="text-align:right;">' . $this->sma->formatMoney($return_sale ? ($inv->product_discount + $return_sale->product_discount) : $inv->product_discount) . '</td>';
                            } ?>
                                <td style="text-align:right; padding-right:10px;"><?= $this->sma->formatMoney($return_sale ? (($inv->total + $inv->product_tax) + ($return_sale->total + $return_sale->product_tax)) : ($inv->total + $inv->product_tax)); ?></td>
                            </tr>
                        <?php
                        } ?> -->
                        <?php
                        if ($return_sale) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang('return_total') . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale->grand_total) . '</td></tr>';
                        }
                        if ($inv->surcharge != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang('return_surcharge') . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->surcharge) . '</td></tr>';
                        }
                        ?>
                       <!-- 
                        <?php if ($inv->order_discount != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang('order_discount') . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . ($inv->order_discount_id ? '<small>(' . $inv->order_discount_id . ')</small> ' : '') . $this->sma->formatMoney($return_sale ? ($inv->order_discount + $return_sale->order_discount) : $inv->order_discount) . '</td></tr>';
                        }
                        ?> -->
                        <!-- <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;">' . lang('order_tax') . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale ? ($inv->order_tax + $return_sale->order_tax) : $inv->order_tax) . '</td></tr>';
                        }
                        ?> -->
                        <!-- <?php if ($inv->shipping != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang('shipping') . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->shipping - ($return_sale && $return_sale->shipping ? $return_sale->shipping : 0)) . '</td></tr>';
                        }
                        ?> -->
                        <tr>
                        <td> </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span>Total (Excluding VAT)</span>
                                (<?= $default_currency->code; ?>)
                                
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;">  <span style="direction: rtl;">الإجمالي (باستثناء ضريبة القيمة المضافة)</span>
                                (<?= $default_currency->code; ?>)
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($return_sale ? ($inv->grand_total + $return_sale->grand_total) : ($inv->total - $inv->product_discount)); ?></td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span>Discount</span>                               
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;">  <span style="direction: rtl;">خصم</span>
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($return_sale ? ($inv->order_discount + $return_sale->order_discount) : $inv->order_discount) ?></td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span>Total Taxable Amount (Excluding 
VAT)(<?= $default_currency->code; ?>)</span>                               
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span style="direction: rtl;">إجمالي المبلغ الخاضع للضريبة (باستثناء ضريبة القيمة المضافة) (USD)</span>
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($return_sale ? ($inv->grand_total + $return_sale->grand_total) - ($inv->order_discount + $return_sale->order_discount) : ($inv->total - $inv->order_discount)); ?></td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span>Total VAT</span>                               
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span style="direction: rtl;">إجمالي ضريبة القيمة المضافة</span>
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($return_sale ? (($inv->grand_total + $return_sale->grand_total) - ($inv->order_discount + $return_sale->order_discount)) * 15 / 100 : ($inv->total - $inv->order_discount)* 15 / 100); ?></td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span>Total Amount Due</span>                               
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span style="direction: rtl;">إجمالي المبلغ المستحق</span>
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($return_sale ? ((($inv->grand_total + $return_sale->grand_total) - ($inv->order_discount + $return_sale->order_discount)) * 15 / 100) + (($inv->grand_total + $return_sale->grand_total) - ($inv->order_discount + $return_sale->order_discount)) : (($inv->total - $inv->order_discount)* 15 / 100) + ($inv->total - $inv->order_discount)); ?></td>
                        </tr>
                        <!-- <tr>
                            <td colspan="<?= $col; ?>"
                                style="text-align:right; font-weight:bold;"><?= lang('paid'); ?>
                                (<?= $default_currency->code; ?>)
                            </td>
                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($return_sale ? ($inv->paid + $return_sale->paid) : $inv->paid); ?></td>
                        </tr>
                        <tr>
                            <td colspan="<?= $col; ?>"
                                style="text-align:right; font-weight:bold;"><?= lang('balance'); ?>
                                (<?= $default_currency->code; ?>)
                            </td>
                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney(($return_sale ? ($inv->grand_total + $return_sale->grand_total) : $inv->grand_total) - ($return_sale ? ($inv->paid + $return_sale->paid) : $inv->paid)); ?></td>
                        </tr> -->

                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="clearfix"></div>

                <div class="col-xs-12">
                    <?php if ($inv->note || $inv->note != '') {
                        ?>
                        <div class="well well-sm">
                            <p class="bold"><?= lang('note'); ?>:</p>

                            <div><?= $this->sma->decode_html($inv->note); ?></div>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-4 pull-left">
                    <p style="height: 80px;"><?= lang('seller'); ?>
                        : <?= $biller->company && $biller->company != '-' ? $biller->company : $biller->name; ?> </p>
                    <hr>
                    <p><?= lang('stamp_sign'); ?></p>
                </div>
                <div class="col-xs-4 pull-right">
                    <p style="height: 80px;"><?= lang('customer'); ?>
                        : <?= $customer->company ? $customer->company : $customer->name; ?> </p>
                    <hr>
                    <p><?= lang('stamp_sign'); ?></p>
                    <?php if ($customer->award_points != 0 && $Settings->each_spent > 0) {
                        ?>
                        <div class="well well-sm">
                            <?=
                            '<p>' . lang('this_sale') . ': ' . floor(($inv->grand_total / $Settings->each_spent) * $Settings->ca_point)
                            . '<br>' .
                            lang('total') . ' ' . lang('award_points') . ': ' . $customer->award_points . '</p>'; ?>
                        </div>
                    <?php
                    } ?>
                </div>
                <div class="clearfix"></div>

        </div>
    </div>
</div>
</body>
</html>
