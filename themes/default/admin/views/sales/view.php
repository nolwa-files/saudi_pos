<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style>
    .qrimg{
        height : 180px;
    }
    .bg-dark{
        background-color: #ddd;
    }
    .text-left{
        text-align: left !important;
    }
    .text-right{
        text-align: right !important;
    }
    @media print {
    @page {
        margin-top: 0;
        margin-bottom: 0;
       
        
    }
    body {
        padding-top: 72px;
        padding-bottom: 72px ;
    }
    @page { size: auto;  margin: 0mm; }
    .padding0100{
        padding: 100px !important;
    }
   
.header, .hide { visibility: hidden }

}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '.sledit', function (e) {
            if (localStorage.getItem('slitems')) {
                e.preventDefault();
                var href = $(this).attr('href');
                bootbox.confirm("<?=lang('you_will_loss_sale_data')?>", function (result) {
                    if (result) {
                        window.location.href = href;
                    }
                });
            }
        });
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-file"></i><?= lang('sale_no') . ' ' . $inv->id; ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang('actions') ?>">
                        </i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <!-- <li>
                            <a href="< ?= admin_url('sales/edit/' . $inv->id) ?>" class="sledit">
                                <i class="fa fa-edit"></i> < ?= lang('edit_sale') ?>
                            </a>
                        </li> -->
                        <li>
                            <a href="<?= admin_url('sales/payments/' . $inv->id) ?>" data-target="#myModal" data-toggle="modal">
                                <i class="fa fa-money"></i> <?= lang('view_payments') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('sales/add_payment/' . $inv->id) ?>" data-target="#myModal" data-toggle="modal">
                                <i class="fa fa-dollar"></i> <?= lang('add_payment') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('sales/email/' . $inv->id) ?>" data-target="#myModal" data-toggle="modal">
                                <i class="fa fa-envelope-o"></i> <?= lang('send_email') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('sales/pdf/' . $inv->id) ?>">
                                <i class="fa fa-file-pdf-o"></i> <?= lang('export_to_pdf') ?>
                            </a>
                        </li>
                        <?php if (!$inv->sale_id) {
                             ?>
                        <li>
                            <a href="<?= admin_url('sales/add_delivery/' . $inv->id) ?>" data-target="#myModal" data-toggle="modal">
                                <i class="fa fa-truck"></i> <?= lang('add_delivery') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('sales/return_sale/' . $inv->id) ?>">
                                <i class="fa fa-angle-double-left"></i> <?= lang('return_sale') ?>
                            </a>
                        </li>
                        <?php
                            } ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <style>
  
        </style>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12" id="printDiv">
                                   
                   
                        <div class="col-xs-12 padding010">
                         <div class="clearfix"></div>
                            <div class="table-responsive" style="">
                                <table style="width:100%;margin-top:192px">
                                 <tr>
                                 <td style="width:30%;padding-left: 12%;"><?=  date("d/m/Y", strtotime($inv->date)); ?></td>
                                 <td style="width:40%;;padding-left: 15%;">TAX INVOICE</td>
                                 <td style="width:30%;padding-left: 20%;"> <?= $inv->id; ?> </td>
                                    
                                 </tr>
                                   <tr><td style="width:100%;padding-left: 20%; padding-top:34px" colspan="3"><?= $customer->company && $customer->company != '-' ? $customer->company : $customer->name; ?></td>
                                 
                                   <tr>
                             </table>

                         </div>
                            
                         </div>
                         
                     
                <table class="table" style="width:100%; margin-top: 15%;" id="myTbl">
                        

                        

                        <tbody>

                        <?php $i=0;
                        foreach ($rows as $row):
                           $i++; ?>
                            <tr style="height: 18px;">
                                <td style="width:8%;text-align:center;font-size:11px"><?=$i?></td>
                                <td style="width:18%;text-align:center;font-size:11px"><?= $row->product_code?></td>
                                <td style="width:23%;text-align:right;white-space:nowrap;overflow:hidden;font-size:11px"> <?= $row->product_name ?></td>
                                    <td style="text-align:right; vertical-align:middle; width:21%;font-size:11px"><?= $this->sma->formatQuantity($row->unit_quantity) ?></td>
                               
                                    <td style="vertical-align:middle;text-align:center;width:18%;font-size:11px">
                                   <?= $this->sma->formatQuantity($row->net_unit_price); ?>
                                </td>
                                <td style="width:20%;text-align:left;font-size:11px">
                                    <?php echo($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) ?>
                                </td>
                                <td style="width:24%;text-align:left;font-size:11px;padding:0 5px 0 0 !important;"><?=$total=$row->unit_quantity*$row->net_unit_price?></td>
                                
                            </tr>
                            <?php
                            $r++;
                        endforeach;
                              
                        if ($return_rows) {
                            echo '<tr class="warning"><td colspan="100%" class="no-border"><strong>' . lang('returned_items') . '</strong></td></tr>';
                            foreach ($return_rows as $row):
                                ?>
                                <tr class="warning" style="height: 18px;">
                                    <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                    <td style="vertical-align:middle;nowrap">
                                        <?= $row->product_code . ' - ' . $row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                        <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                        <?= $row->details ? '<br>' . $row->details : ''; ?>
                                    </td>
                                    <?php if ($Settings->indian_gst) {
                                    ?>
                                    <td style="width: 80px; text-align:center; vertical-align:middle;"><<?= $row->hsn_code ?: ''; ?></td>
                                    <?php
                                } ?>
                                    <td style="width: 100px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity) . ' ' . $row->base_unit_code; ?></td>
                                    <?php
                                    if ($Settings->product_serial) {
                                        echo '<td>' . $row->serial_no . '</td>';
                                    } ?>
                                    <td style="text-align:right; width:120px; padding-right:10px;"><?= $this->sma->formatMoney($row->unit_price); ?></td>
                                    <?php
                                    if ($Settings->tax1 && $inv->product_tax > 0) {
                                        echo '<td style="width: 120px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>(' . ($Settings->indian_gst ? $row->tax : $row->tax_code) . ')</small>' : '') . ' ' . $this->sma->formatMoney($row->item_tax) . '</td>';
                                    }
                            if ($Settings->product_discount && $inv->product_discount != 0) {
                                echo '<td style="width: 120px; text-align:left; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) . '</td>';
                            } ?>
                                    <td style="text-align:right; width:120px; padding-right:10px;"><?= $this->sma->formatMoney($row->subtotal); ?></td>
                                </tr>
                                <?php
                                $r++;
                            endforeach;
                        }

                        ?>        <?php for ($x = $r; $x <= 18; $x++) { ?>
                        <tr style="height: 18px;">
                        <td style="height: 18px;">&nbsp;</td>
                        <td style="height: 18px;">&nbsp;</td>
                        <td style="height: 18px;">&nbsp;</td>
                        <td style="height: 18px;">&nbsp;</td>
                        <td style="height: 18px;">&nbsp;</td>
                        <td style="height: 18px;">&nbsp;</td>
                        <td style="height: 18px;">&nbsp;</td>
                        </tr>
                      <?php  } ?> 
                        </tbody>
                    </table>
                    <table style="width: 100%;">
                        <tbody>
                       
                        <tr>
                         <td style="margin-top: 5%; padding:15px 0 0 35px !important">
                           <?php 
                            $inv_date = date("Y-m-d h:i:s", strtotime($inv->date));
                            $vat_tls = $return_sale ? (($inv->grand_total + $return_sale->grand_total) - ($inv->order_discount + $return_sale->order_discount)) * 15 / 100 : ($inv->total - $inv->order_discount)* 15 / 100;
                            $grand_tls = $return_sale ? ((($inv->grand_total + $return_sale->grand_total) - ($inv->order_discount + $return_sale->order_discount)) * 15 / 100) + (($inv->grand_total + $return_sale->grand_total) - ($inv->order_discount + $return_sale->order_discount)) : (($inv->total - $inv->order_discount)* 15 / 100) + ($inv->total - $inv->order_discount);
                            $vat_total = number_format((float)$vat_tls, 2, '.', '');
                            $grand_total = number_format((float)$grand_tls, 2, '.', '');
                            $seller = $biller->company;
                            $result = chr(1) . chr( strlen($seller) ) . $seller;
                            $result.= chr(2) . chr( strlen($biller->vat_no) ) . $biller->vat_no;
                            $result.= chr(3) . chr( strlen($inv_date) ) . $inv_date;
                            $result.= chr(4) . chr( strlen($grand_total) ) . $grand_total;
                            $result.= chr(5) . chr( strlen($vat_total) ) . $vat_total;
                            $newQr = base64_encode($result); ?>
                            <?= $this->sma->qrcode('url', $newQr, 0); ?>
                       </td>
                        <td colspan="6">
                            <table style="width:100%;margin-top:9%;>
                                <tr style="height: 22px;">
                                    <td style="width:70%"></td>
                                    <td 
                                        style="text-align:right; font-weight:bold; margin-bottom:20px;padding:0 5px 0 0 !important;"> 
                                        <?=$subtot= $this->sma->formatMoney($return_sale ? ($inv->grand_total + $return_sale->grand_total) : ($inv->total - $inv->product_discount)); ?></td>
                                </tr>
                                <tr style="height: 40px;">
                                    <td  style="width:70%"></td>
                                    <td style="text-align:right; font-weight:bold;padding:0 5px 0 0 !important;">  <?= $dis= ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) ?></td>
                                </tr>
                                <tr style="height: 28px;">
                                    <td style="width:70%"></td>
                                    <td
                                        style="text-align:right; font-weight:bold;padding:0 5px 0 0 !important;"> <?=$subtot+$dis?>
                                    </td>
                                 </tr>
                                
                                 <tr style="height: 43px;">
                                     <td style="width:70%"></td>
                                     <td style="text-align:right; font-weight:bold;padding:0 5px 0 0 !important;"><?= $this->sma->formatMoney($return_sale ? (($inv->grand_total + $return_sale->grand_total) - ($inv->order_discount + $return_sale->order_discount)) * 15 / 100 : ($inv->total - $inv->order_discount)* 15 / 100); ?></td>
                                </tr>
                                <tr style="height: 20px;">
                                     <td style="width:70%"></td>
                                     <td style="text-align:right; font-weight:bold;padding:0 5px 0 0 !important;"><?= $this->sma->formatMoney($return_sale ? ((($inv->grand_total + $return_sale->grand_total) - ($inv->order_discount + $return_sale->order_discount)) * 15 / 100) + (($inv->grand_total + $return_sale->grand_total) - ($inv->order_discount + $return_sale->order_discount)) : (($inv->total - $inv->order_discount)* 15 / 100) + ($inv->total - $inv->order_discount)); ?></td>
                                </tr>
                             </table>
                            

                        
                    </table>


              
            </div>
        </div>
      
                   
        <?php if (!$Supplier || !$Customer) {
                            ?>
            <div class="buttons">
                <div class="btn-group btn-group-justified">
                    <div class="btn-group">
                        <a href="<?= admin_url('sales/payments/' . $inv->id) ?>" data-toggle="modal" data-target="#myModal" class="tip btn btn-primary tip" title="<?= lang('view_payments') ?>">
                            <i class="fa fa-money"></i> <span class="hidden-sm hidden-xs"><?= lang('view_payments') ?></span>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a href="<?= admin_url('sales/add_payment/' . $inv->id) ?>" data-toggle="modal" data-target="#myModal" class="tip btn btn-primary tip" title="<?= lang('add_payment') ?>">
                            <i class="fa fa-money"></i> <span class="hidden-sm hidden-xs"><?= lang('add_payment') ?></span>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a href="<?= admin_url('sales/email/' . $inv->id) ?>" data-toggle="modal" data-target="#myModal" class="tip btn btn-primary tip" title="<?= lang('email') ?>">
                            <i class="fa fa-envelope-o"></i> <span class="hidden-sm hidden-xs"><?= lang('email') ?></span>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a href="javascript:void(0)" class="tip btn btn-primary printDivs" title="<?= lang('print') ?>">
                            <i class="fa fa-download"></i> <span class="hidden-sm hidden-xs"><?= lang('print') ?></span>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a href="<?= admin_url('sales/pdf/' . $inv->id) ?>" class="tip btn btn-primary" title="<?= lang('download_pdf') ?>">
                            <i class="fa fa-download"></i> <span class="hidden-sm hidden-xs"><?= lang('pdf') ?></span>
                        </a>
                    </div>
                    <?php if (!$inv->sale_id) {
                                ?>
                    <div class="btn-group">
                        <a href="<?= admin_url('sales/add_delivery/' . $inv->id) ?>" data-toggle="modal" data-target="#myModal" class="tip btn btn-primary tip" title="<?= lang('add_delivery') ?>">
                            <i class="fa fa-truck"></i> <span class="hidden-sm hidden-xs"><?= lang('add_delivery') ?></span>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a href="<?= admin_url('sales/edit/' . $inv->id) ?>" class="tip btn btn-warning tip sledit" title="<?= lang('edit') ?>">
                            <i class="fa fa-edit"></i> <span class="hidden-sm hidden-xs"><?= lang('edit') ?></span>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a href="#" class="tip btn btn-danger bpo"
                            title="<b><?= $this->lang->line('delete_sale') ?></b>"
                            data-content="<div style='width:150px;'><p><?= lang('r_u_sure') ?></p><a class='btn btn-danger' href='<?= admin_url('sales/delete/' . $inv->id) ?>'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button></div>"
                            data-html="true" data-placement="top"><i class="fa fa-trash-o"></i>
                            <span class="hidden-sm hidden-xs"><?= lang('delete') ?></span>
                        </a>
                    </div>
                    <?php
                            } ?>
                    <!--<div class="btn-group"><a href="<?= admin_url('sales/excel/' . $inv->id) ?>" class="tip btn btn-primary"  title="<?= lang('download_excel') ?>"><i class="fa fa-download"></i> <?= lang('excel') ?></a></div>-->
                </div>
            </div>
        <?php
                        } ?>
    </div>
</div>
<script>
   $( document ).ready(function() {
   
    $(".printDivs").click(function () {
        var divToPrint=document.getElementById("printDiv");
    var htmlToPrint = '' +
        '<style type="text/css">' +
       'table td {' +
        'diplay:inline' +
        'padding;0.5em;' +
        '}' +
        'table tr {' +
        '' +
        'padding;0.5em;' +
        '}' +
        
        '</style>';
    htmlToPrint += divToPrint.outerHTML;
    newWin = window.open("");
    newWin.document.write(htmlToPrint);
    newWin.print();
    newWin.close();
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
<<<<<<< HEAD
  
=======
>>>>>>> cd933e8a3e094646e148069aa238ea70706999fa
        })
});



    </script>